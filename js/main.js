(function() {

  $(function() {
  
    $(".job_type_button").live('click', function(event) {
      return event.preventDefault();
    });
    $(".print_size_options").live('change', function(event) {
      var selected;
      selected = $(this).val();
      if (selected === "customSize") {
        return $(this).parent().find(".custom_size").fadeIn();
      } else {
        return $(this).parent().find(".custom_size").fadeOut();
      }
    });
    $(".binding_options").change(function(evt) {
      var selected;
      selected = $(this).val();
      if (selected === "plasticCoil") {
        $(this).parent().find(".plasticCoil").fadeIn();
      } else {
        $(this).parent().find(".plasticCoil").fadeOut();
      }
      if (selected === "tape") {
        $(this).parent().find(".tapeColor").fadeIn();
      } else {
        $(this).parent().find(".tapeColor").fadeOut();
      }
      if (selected === "wireo") {
        return $(this).parent().find(".wireColor").fadeIn();
      } else {
        return $(this).parent().find(".wireColor").fadeOut();
      }
    }).change();
    $(".fileDelivery").change(function(evt) {
      var selected;
      selected = $(this).val();
      if (selected === "viaftp") {
        return $(".emailFTP").fadeIn();
      } else {
        return $(".emailFTP").fadeOut();
      }
    }).change();
    $("#deliveryOptions").change(function(evt) {
      var selected;
      selected = $(this).val();
      if (selected === "delivery") {
        return $(".required_delivery").fadeIn('slow');
      } else if (selected === "upsground") {
        return $(".required_delivery").fadeIn('slow');
      } else if (selected === "upssecondday") {
        return $(".required_delivery").fadeIn('slow');
      } else if (selected === "upsovernight") {
        return $(".required_delivery").fadeIn('slow');
      } else {
        return $(".required_delivery").fadeOut('slow');
      }
    }).change();
    $("#copy").change(function() {
      if (this.checked) {
        return $("#copy").click(function() {
          var billing, shipping;
          billing = $("input[name='billing_info'], select[name='billing_info']");
          shipping = $("input[name='shipping_info'], select[name='shipping_info']");
          return billing.each(function(i, o) {
            return $(shipping.get(i)).val($(o).val());
          });
        });
      } else {
        return $("#copy").click(function() {
          if (!$(this).is(":checked")) {
            return $(".clearForm").val(" ");
          }
        });
      }
    });
    $("#remove").click(function() {
      return $('#deliverToForm').slideToggle();
    });
    return $("#remove").change(function() {
      if (this.checked) {
        return $("#remove").click(function() {
          if (!$(this).is(":checked")) {
            return $(".clearForm").val(" ");
          }
        });
      }
    });
  });

}).call(this);
